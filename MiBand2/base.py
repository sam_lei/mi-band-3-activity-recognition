import struct
import time
import binascii
import logging
from datetime import datetime
from Crypto.Cipher import AES
from queue import Queue, Empty
from bluepy.btle import Peripheral, DefaultDelegate, ADDR_TYPE_RANDOM, BTLEException


from constants import UUIDS, AUTH_STATES, ALERT_TYPES, QUEUE_TYPES, COMMAND


class AuthenticationDelegate(DefaultDelegate):

    """This Class inherits DefaultDelegate to handle the authentication process."""

    def __init__(self, device):
        DefaultDelegate.__init__(self)
        self.device = device

    def handleNotification(self, hnd, data):
        # Debug purposes
        self.device.log.debug(hex(hnd) + " " + str(binascii.hexlify(data)) + " " + str(len(data)))

        if hnd == self.device.charAuth.getHandle():
            self.handleAuth(data)
        elif hnd == self.device.charHeartMeasure.getHandle():
            self.handleHeart(data)
        elif hnd == 0x35 and data[0:1] == b'\x01':
            self.handleRawAccel(data)
        elif hnd == 0x35 and data[0:1] == b'\x02':
            self.handleRawHeart(data)
        elif hnd == 0x35 and len(data) == 11:
            self.handleUnknown(data)
        else:
            self.handleNotRecognized(hnd, data)

    def handleAuth(self, data):
        if data[:3] == b'\x10\x01\x01':
            self.device._req_rdn()
        elif data[:3] == b'\x10\x01\x04':
            self.device.state = AUTH_STATES.KEY_SENDING_FAILED
        elif data[:3] == b'\x10\x02\x01':
            # 16 bytes
            random_nr = data[3:]
            self.device._send_enc_rdn(random_nr)
        elif data[:3] == b'\x10\x02\x04':
            self.device.state = AUTH_STATES.REQUEST_RN_ERROR
        elif data[:3] == b'\x10\x03\x01':
            self.device.state = AUTH_STATES.AUTH_OK
        elif data[:3] == b'\x10\x03\x04':
            self.device.status = AUTH_STATES.ENCRIPTION_KEY_FAILED
            self.device._send_key()
        else:
            self.device.state = AUTH_STATES.AUTH_FAILED

    def handleRawAccel(self, data):
        self.device.queue.put((QUEUE_TYPES.RAW_ACCEL, data))

    def handleRawHeart(self, data):
        self.device.queue.put((QUEUE_TYPES.RAW_HEART, data))

    def handleHeart(self, data):
        self.device.queue.put((QUEUE_TYPES.HEART, data))

    def handleUnknown(self, data):
        self.device.queue.put((QUEUE_TYPES.RAW_UNKNOWN, data))

    def handleNotRecognized(self, hnd, data):
        self.device.log.error("Unhandled Response " + hex(hnd) + ": " +
                              str(binascii.hexlify(data)) + " len:" + str(len(data)))


class MiBand2(Peripheral):
    # _KEY = b'\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x40\x41\x42\x43\x44\x45'
    # _send_key_cmd = struct.pack('<18s', b'\x01\x08' + _KEY)
    # _send_rnd_cmd = struct.pack('<2s', b'\x02\x08')
    # _send_enc_key = struct.pack('<2s', b'\x03\x08')
    _KEY = b'\xf5\xd2\x29\x87\x65\x0a\x1d\x82\x05\xab\x82\xbe\xb9\x38\x59\xcf'
    _send_key_cmd = struct.pack('<18s', b'\x01\x00' + _KEY)
    _send_rnd_cmd = struct.pack('<2s', b'\x02\x00')
    _send_enc_key = struct.pack('<2s', b'\x03\x00')

    def __init__(self, mac_address, timeout=0.5, debug=False):
        FORMAT = '%(asctime)-15s %(name)s (%(levelname)s) > %(message)s'
        logging.basicConfig(format=FORMAT)
        logLevel = logging.WARNING if not debug else logging.DEBUG
        self.log = logging.getLogger(self.__class__.__name__)
        self.log.setLevel(logLevel)

        self.log.info('Connecting to ' + mac_address)
        while True:
            try:
                Peripheral.__init__(self, mac_address, addrType=ADDR_TYPE_RANDOM)
                break
            except BTLEException:
                self.log.info("Connection failed, trying again..")
        self.log.info('Connected')

        self.timeout = timeout
        self.mac_address = mac_address
        self.state = None
        self.queue = Queue()
        self.heart_measure_callback = None
        self.heart_raw_callback = None
        self.accel_raw_callback = None
        self.unknown_raw_callback = None
        self.last_th = 0
        self.curr_th = 0
        self.last_ta = 0
        self.curr_ta = 0

        self.svc_1 = self.getServiceByUUID(UUIDS.SERVICE_MIBAND1)
        self.svc_2 = self.getServiceByUUID(UUIDS.SERVICE_MIBAND2)
        self.svc_heart = self.getServiceByUUID(UUIDS.GATT_HEART_RATE)

        self.charAuth = self.svc_2.getCharacteristics(UUIDS.CHARACTERISTIC_AUTH)[0]
        self.descAuth = self.charAuth.getDescriptors(forUUID=UUIDS.NOTIFICATION_DESCRIPTOR)[0]

        self.charHeartCtrl = self.svc_heart.getCharacteristics(UUIDS.SERVICE_HEART_RATE_CONTROL)[0]
        self.charHeartMeasure = self.svc_heart.getCharacteristics(UUIDS.SERVICE_HEART_RATE_MEASURE)[0]

        # Enable auth service notifications on startup
        self._auth_notif(True)
        # Let MiBand2 to settle
        self.waitForNotifications(0.1)

    # Auth helpers ######################################################################

    def _auth_notif(self, enabled):
        if enabled:
            self.log.info("Enabling Auth Service notifications status...")
            self.descAuth.write(b"\x01\x00", True)
        elif not enabled:
            self.log.info("Disabling Auth Service notifications status...")
            self.descAuth.write(b"\x00\x00", True)
        else:
            self.log.error("Something went wrong while changing the Auth Service notifications status...")

    def _encrypt(self, message):
        aes = AES.new(self._KEY, AES.MODE_ECB)
        return aes.encrypt(message)

    def _send_key(self):
        self.log.info("Sending Key...")
        self.charAuth.write(self._send_key_cmd)
        self.waitForNotifications(self.timeout)

    def _req_rdn(self):
        self.log.info("Requesting random number...")
        self.charAuth.write(self._send_rnd_cmd)
        self.waitForNotifications(self.timeout)

    def _send_enc_rdn(self, data):
        self.log.info("Sending encrypted random number")
        cmd = self._send_enc_key + self._encrypt(data)
        send_cmd = struct.pack('<18s', cmd)
        self.charAuth.write(send_cmd)
        self.waitForNotifications(self.timeout)

    # Parse helpers ###################################################################

    def _parse_raw_accel(self, bytes):
        # format in bytes: 2time, x*(2x-val, 2y-val, 2z-val)
        res = []

        # normalize time (system time + offset between measurements)
        ta = struct.unpack('H', bytes[0:2])[0]
        ta_max = struct.unpack('H', b'\xff\xff')[0]
        if ta < self.last_ta:
            # handle unsigned short int overflow
            self.curr_ta += ta + ta_max - self.last_ta
        elif self.last_ta != 0:
            self.curr_ta += ta - self.last_ta

        self.last_ta = ta

        for i in range(int((len(bytes)-2) / 6)):
            g = struct.unpack('hhh', bytes[2 + i * 6:8 + i * 6])
            res.append({'t': self.curr_ta / 3000, 'x': g[0], 'y': g[1], 'z': g[2]})
        return res

    def _parse_raw_heart(self, bytes):
        # format in bytes: 2time, x*2val
        res = []

        # normalize time (system time + offset between measurements)
        th = struct.unpack('H', bytes[0:2])[0]
        th_max = struct.unpack('H', b'\xff\xff')[0]
        if th < self.last_th:
            # handle unsigned short int overflow
            self.curr_th += th + th_max - self.last_th
        elif self.last_th != 0:
            self.curr_th += th - self.last_th

        self.last_th = th

        for i in range(int((len(bytes)-2) / 2)):
            m = struct.unpack('H', bytes[2 + i * 2:4 + i * 2])
            res.append({'t': self.curr_th / 1000, 'm': m[0]})
        return res

    def _parse_raw_unknown(self, data):
        # lengths: 11
        meta = struct.unpack('BB', data[1:3])
        t = struct.unpack('i', data[3:7])
        return {'c': meta[0], 'x': meta[1], 't': t[0] / 1000}

    def _parse_date(self, bytes):
        # not sure if bytes need conversion to int
        year = struct.unpack('h', bytes[0:2])[0] if len(bytes) >= 2 else None
        month = bytes[2] if len(bytes) >= 3 else None
        day = bytes[3] if len(bytes) >= 4 else None
        hours = bytes[4] if len(bytes) >= 5 else None
        minutes = bytes[5] if len(bytes) >= 6 else None
        seconds = bytes[6] if len(bytes) >= 7 else None
        day_of_week = bytes[7] if len(bytes) >= 8 else None
        fractions256 = bytes[8] if len(bytes) >= 9 else None

        return {"date": datetime(*(year, month, day, hours, minutes, seconds)), "day_of_week": day_of_week, "fractions256": fractions256}

    def _parse_battery_response(self, data):
        level = data[1] if len(data) >= 2 else None
        last_level = data[19] if len(data) >= 20 else None
        status = 'normal' if data[2] == 0 else "charging"
        datetime_last_charge = self._parse_date(data[11:18])
        datetime_last_off = self._parse_date(data[3:10])

        # WTF?
        # struct.unpack('b', bytes[10])
        # struct.unpack('b', bytes[18])
        # print struct.unpack('b', bytes[10]), struct.unpack('b', bytes[18])

        res = {
            "status": status,
            "level": level,
            "last_level": last_level,
            "last_charge": datetime_last_charge,
            "last_off": datetime_last_off
        }
        return res

    # Queue ###################################################################

    def _get_from_queue(self, _type):
        try:
            res = self.queue.get(False)
        except Empty:
            return None
        if res[0] != _type:
            self.queue.put(res)
            return None
        return res[1]

    def _parse_queue(self):
        while True:
            try:
                res = self.queue.get(False)
                _type = res[0]
                if self.heart_measure_callback and _type == QUEUE_TYPES.HEART:
                    self.heart_measure_callback(struct.unpack('bb', res[1])[1])
                elif self.heart_raw_callback and _type == QUEUE_TYPES.RAW_HEART:
                    self.heart_raw_callback(self._parse_raw_heart(res[1]))
                elif self.accel_raw_callback and _type == QUEUE_TYPES.RAW_ACCEL:
                    self.accel_raw_callback(self._parse_raw_accel(res[1]))
                elif self.unknown_raw_callback and _type == QUEUE_TYPES.RAW_UNKNOWN:
                    self.unknown_raw_callback(self._parse_raw_unknown(res[1]))
            except Empty:
                break

    # API ####################################################################

    def initialize(self):
        self.setDelegate(AuthenticationDelegate(self))
        self._send_key()

        while True:
            self.waitForNotifications(0.1)
            if self.state == AUTH_STATES.AUTH_OK:
                self.log.info('Initialized')
                self._auth_notif(False)
                return True
            elif self.state is None:
                continue

            self.log.error(self.state)
            return False

    def authenticate(self):
        self.setDelegate(AuthenticationDelegate(self))
        self._req_rdn()

        while True:
            self.waitForNotifications(0.1)
            if self.state == AUTH_STATES.AUTH_OK:
                self.log.info('Authenticated')
                return True
            elif self.state is None:
                continue

            self.log.error(self.state)
            return False

    def get_battery_info(self):
        char = self.svc_1.getCharacteristics(UUIDS.CHARACTERISTIC_BATTERY)[0]
        return self._parse_battery_response(char.read())

    def get_current_time(self):
        char = self.svc_1.getCharacteristics(UUIDS.SERVICE_CURRENT_TIME)[0]
        return self._parse_date(char.read()[0:9])

    def get_revision(self):
        svc = self.getServiceByUUID(UUIDS.GATT_DEVICE_INFO)
        char = svc.getCharacteristics(UUIDS.CHARACTERISTIC_REVISION)[0]
        data = char.read()
        revision = struct.unpack('9s', data[-9:])[0] if len(data) == 9 else None
        return revision

    def get_hrdw_revision(self):
        svc = self.getServiceByUUID(UUIDS.GATT_DEVICE_INFO)
        char = svc.getCharacteristics(UUIDS.CHARACTERISTIC_HRDW_REVISION)[0]
        data = char.read()
        revision = struct.unpack('8s', data[-8:])[0] if len(data) == 8 else None
        return revision

    def set_encoding(self, encoding="en_US"):
        char = self.svc_1.getCharacteristics(UUIDS.CHARACTERISTIC_CONFIGURATION)[0]
        packet = struct.pack('5s', encoding)
        packet = b'\x06\x17\x00' + packet
        return char.write(packet)

    def set_heart_monitor_sleep_support(self, enabled=True, measure_minute_interval=1):
        char_m = self.svc_heart.getCharacteristics(UUIDS.SERVICE_HEART_RATE_MEASURE)[0]
        char_d = char_m.getDescriptors(forUUID=UUIDS.NOTIFICATION_DESCRIPTOR)[0]
        char_d.write(b'\x01\x00', True)
        self.charHeartCtrl.write(b'\x15\x00\x00', True)
        # measure interval set to off
        self.charHeartCtrl.write(b'\x14\x00', True)
        if enabled:
            self.charHeartCtrl.write(b'\x15\x00\x01', True)
            # measure interval set
            self.charHeartCtrl.write(b'\x14' + str(measure_minute_interval).encode(), True)
        char_d.write(b'\x00\x00', True)

    def get_serial(self):
        svc = self.getServiceByUUID(UUIDS.GATT_DEVICE_INFO)
        char = svc.getCharacteristics(UUIDS.CHARACTERISTIC_SERIAL)[0]
        data = char.read()
        serial = struct.unpack('12s', data[-12:])[0] if len(data) == 12 else None
        return serial

    def get_steps(self):
        char = self.svc_1.getCharacteristics(UUIDS.CHARACTERISTIC_STEPS)[0]
        a = char.read()
        steps = struct.unpack('h', a[1:3])[0] if len(a) >= 3 else None
        meters = struct.unpack('h', a[5:7])[0] if len(a) >= 7 else None
        fat_gramms = struct.unpack('h', a[2:4])[0] if len(a) >= 4 else None
        # why only 1 byte??
        callories = struct.unpack('b', a[9])[0] if len(a) >= 10 else None
        return {
            "steps": steps,
            "meters": meters,
            "fat_gramms": fat_gramms,
            "callories": callories

        }

    def send_alert(self, _type):
        svc = self.getServiceByUUID(UUIDS.SERVICE_ALERT)
        char = svc.getCharacteristics(UUIDS.SERVICE_ALERT)[0]
        char.write(_type)

    def get_heart_rate_one_time(self):
        # stop continous
        self.charHeartCtrl.write(b'\x15\x01\x00', True)
        # stop manual
        self.charHeartCtrl.write(b'\x15\x02\x00', True)
        # start manual
        self.charHeartCtrl.write(b'\x15\x02\x01', True)
        res = None
        while not res:
            self.waitForNotifications(self.timeout)
            res = self._get_from_queue(QUEUE_TYPES.HEART)

        rate = struct.unpack('bb', res)[1]
        return rate

    def start_heart_rate_realtime(self, heart_measure_callback):
        char_m = self.svc_heart.getCharacteristics(UUIDS.SERVICE_HEART_RATE_MEASURE)[0]
        char_d = char_m.getDescriptors(forUUID=UUIDS.NOTIFICATION_DESCRIPTOR)[0]
        char_ctrl = self.svc_heart.getCharacteristics(UUIDS.SERVICE_HEART_RATE_CONTROL)[0]

        self.heart_measure_callback = heart_measure_callback

        # stop heart monitor continues & manual
        char_ctrl.write(b'\x15\x02\x00', True)
        char_ctrl.write(b'\x15\x01\x00', True)
        # enable heart monitor notifications
        char_d.write(b'\x01\x00', True)
        # start hear monitor continues
        char_ctrl.write(b'\x15\x01\x01', True)
        t = time.time()
        while True:
            self.waitForNotifications(0.5)
            self._parse_queue()
            # send ping request every 12 sec
            if (time.time() - t) >= 12:
                char_ctrl.write(b'\x16', True)
                t = time.time()

    def start_raw_data_realtime(self, heart_measure_callback=None, heart_raw_callback=None, accel_raw_callback=None, unknown_raw_callback=None):
        # heartrate
        char_heart = self.svc_heart.getCharacteristics(UUIDS.SERVICE_HEART_RATE_MEASURE)[0]
        char_heart_d = char_heart.getDescriptors(forUUID=UUIDS.NOTIFICATION_DESCRIPTOR)[0]
        # accelerometer
        char_accel = self.svc_1.getCharacteristics(UUIDS.CHARACTERISTIC_ACCEL)[0]
        char_accel_d = char_accel.getDescriptors(forUUID=UUIDS.NOTIFICATION_DESCRIPTOR)[0]

        # control
        char_ctrl = self.svc_heart.getCharacteristics(UUIDS.SERVICE_HEART_RATE_CONTROL)[0]

        # callbacks
        if heart_measure_callback:
            self.heart_measure_callback = heart_measure_callback
        if heart_raw_callback:
            self.heart_raw_callback = heart_raw_callback
        if accel_raw_callback:
            self.accel_raw_callback = accel_raw_callback
        if unknown_raw_callback:
            self.unknown_raw_callback = unknown_raw_callback

        # stop heart monitor continues & manual
        char_ctrl.write(b'\x15\x02\x00', True)
        char_ctrl.write(b'\x15\x01\x00', True)

        # enabling accelerometer & heart monitor raw data notifications
        char_accel.write(b'\x01\x03\x19')

        # IMO: enablee heart monitor notifications
        char_heart_d.write(b'\x01\x00', True)

        # start heart monitor continues
        char_ctrl.write(b'\x15\x01\x01', True)

        # WTF
        char_accel.write(b'\x02')

        t = time.time()
        t2 = time.time()
        while True:
            # get and parse data
            self.waitForNotifications(1.0)
            self._parse_queue()

            # send ping request every 12 sec to keep heart measure alive
            if (time.time() - t) >= 12:
                char_ctrl.write(b'\x16', True)
                t = time.time()

            # send ping request every 30 sec to keep accel alive
            if (time.time() - t2) >= 30:
                # char_accel.write(b'\x02')
                t2 = time.time()

    def stop_realtime(self):
        char_m = self.svc_heart.getCharacteristics(UUIDS.SERVICE_HEART_RATE_MEASURE)[0]
        char_d = char_m.getDescriptors(forUUID=UUIDS.NOTIFICATION_DESCRIPTOR)[0]
        char_ctrl = self.svc_heart.getCharacteristics(UUIDS.SERVICE_HEART_RATE_CONTROL)[0]

        char_sensor1 = self.svc_1.getCharacteristics(UUIDS.CHARACTERISTIC_HZ)[0]
        char_sens_d1 = char_sensor1.getDescriptors(forUUID=UUIDS.NOTIFICATION_DESCRIPTOR)[0]

        char_sensor2 = self.svc_1.getCharacteristics(UUIDS.CHARACTERISTIC_ACCEL)[0]

        # stop heart monitor continues
        char_ctrl.write(b'\x15\x01\x00', True)
        char_ctrl.write(b'\x15\x01\x00', True)
        # IMO: stop heart monitor notifications
        char_d.write(b'\x00\x00', True)
        # WTF
        char_sensor2.write(b'\x03')
        # IMO: stop notifications from sensors
        char_sens_d1.write(b'\x00\x00', True)

        self.heart_measure_callback = None
        self.heart_raw_callback = None
        self.accel_raw_callback = None


    def start_sensor_stream(self, heart_measure_callback=None, heart_raw_callback=None, accel_raw_callback=None, unknown_raw_callback=None):
        # heartrate
        # accelerometer
        char_accel = self.svc_1.getCharacteristics(UUIDS.CHARACTERISTIC_SENSOR_DATA)[0]

        # callbacks
        if heart_measure_callback:
            self.heart_measure_callback = heart_measure_callback
        if heart_raw_callback:
            self.heart_raw_callback = heart_raw_callback
        if accel_raw_callback:
            self.accel_raw_callback = accel_raw_callback
        if unknown_raw_callback:
            self.unknown_raw_callback = unknown_raw_callback

        char_accel.write(COMMAND.COMMAND_GET_SENSOR_DATA)

        # WTF
        # char_accel.write(b'\x02')

        while True:
            # get and parse data
            self.waitForNotifications(1.0)
            self._parse_queue()
