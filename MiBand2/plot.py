import matplotlib.pyplot as plt
import csv
import pandas as pd
import numpy as np

t1 = []
x = []
y = []
z = []
t2 = []
h2 = []
t3 = []
h3 = []


def moving_average(N, list):
    cumsum, moving_aves = [0], []
    for i, x in enumerate(list, 1):
        cumsum.append(cumsum[i-1] + x)
        if i >= N:
            moving_ave = (cumsum[i] - cumsum[i-N])/N
            #can do stuff with moving_ave here
            moving_aves.append(moving_ave)
    return moving_aves

def diff(time, list):
    time_diff = np.diff(time)
    list_diff = np.diff(list)
    diff = []
    for i in range(len(list_diff)-1):
        if time_diff[i] != 0:
            diff.append(list_diff[i] / time_diff[i])
        else:
            diff.append(0)
    return diff

def bpm(diff):
    for i in range(len(h2_ma)-2):
        if h2_ma[i] < h2_ma[i+1] and h2_ma[i+1] > h2_ma[i+2]:
            i = 2


with open('accel.csv', 'r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    for row in plots:
        t1.append(float(row[0]))
        x.append(int(row[1]))
        y.append(int(row[2]))
        z.append(int(row[3]))

with open('heart.csv', 'r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    for row in plots:
        t2.append(float(row[0]))
        h2.append(int(row[1]))

with open('rheart.csv', 'r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    for row in plots:
        t3.append(float(row[0]))
        h3.append(int(row[1]))

h2_ma = moving_average(10, h2)
h2_diff = diff(t2, h2_ma)
h2_diff_ma = moving_average(10, h2_diff)



plt.figure(1)
ax1 = plt.subplot(311)
plt.plot(t1, x, label='x-accel')
plt.plot(t1, y, label='y-accel')
plt.plot(t1, z, label='z-accel')
ax1.legend(loc='upper right')

ax2 = plt.subplot(312, sharex=ax1)
plt.plot(t2, h2, label='ppg')
plt.plot(t2[9:], h2_ma, label='ppg mv10')
ax2.legend(loc='upper right')

ax3 = plt.subplot(313, sharex=ax1)
# plt.plot(t3, h3, label='heart')
plt.plot(t2[11:], h2_diff, label='diff')
plt.plot(t2[20:], h2_diff_ma, label='diff ma')
plt.plot(t2[29:], moving_average(10, h2_diff_ma), label='diff ma2')
ax3.legend(loc='upper right')
plt.show()
