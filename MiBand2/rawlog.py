import sys
import time
import os
from base import MiBand2
from bluepy.btle import BTLEException

# MAC = sys.argv[1]
MAC = 'ED:FF:28:DD:50:5B'

band = MiBand2(MAC, debug=True)
band.setSecurityLevel(level="medium")


def current_milli_time():
    return int(round(time.time() * 1000)) / 1000


accel_path = 'accel.csv'  # sys.argv[2]
heart_path = 'heart.csv'  # sys.argv[2]
rheart_path = 'rheart.csv'  # sys.argv[2]
if os.path.exists(accel_path):
    os.remove(accel_path)
if os.path.exists(heart_path):
    os.remove(heart_path)
if os.path.exists(rheart_path):
    os.remove(rheart_path)

fp_accel = open(accel_path, 'a')
# fp_accel.write('t, x, y, z\n')
fp_heart = open(heart_path, 'a')
# fp_heart.write('t, h\n')
fp_rheart = open(rheart_path, 'a')
# fp_rheart.write('t, h\n')

last_hearts_time = 0
last_accels_time = 0

hearts = []
accels = []

if len(sys.argv) > 1:
    if band.initialize():
        print("Init OK")
    band.set_heart_monitor_sleep_support(enabled=False)
    band.disconnect()
    sys.exit(0)
else:
    band.authenticate()


def l(x):
    print('Realtime heart:', x)
    data = "%.3f, %s\n" % (current_milli_time(), x)
    fp_rheart.write(data)


def b(x):
    print('Raw heart:', x)
    hearts.extend(x)


def f(x):
    print('Raw accel:', x)
    accels.extend(x)


def g(x):
    global last_accels_time
    global accels
    global fp_accel

    global last_hearts_time
    global hearts
    global fp_heart

    state = x['x']
    if state == 1:
        if last_accels_time != 0:
            diff = current_milli_time() - last_accels_time
            for i in range(len(accels)):
                accels[i]['tt'] = current_milli_time() - (diff / len(accels)) * (len(accels) - i)
                data = "%.3f, %s, %s, %s\n" % (accels[i]['tt'], accels[i]['x'], accels[i]['y'], accels[i]['z'])
                fp_accel.write(data)
        accels = []
        last_accels_time = current_milli_time()

    if state == 2:
        if last_hearts_time != 0:
            diff = current_milli_time() - last_hearts_time
            for i in range(len(hearts)):
                hearts[i]['tt'] = current_milli_time() - (diff / len(hearts)) * (len(hearts) - i)
                data = "%.3f, %s\n" % (hearts[i]['tt'], hearts[i]['m'])
                fp_heart.write(data)
        hearts = []
        last_hearts_time = current_milli_time()


# band.start_raw_data_realtime(unknown_raw_callback=g)
try:
    band.start_sensor_stream(heart_measure_callback=l, heart_raw_callback=b, accel_raw_callback=f, unknown_raw_callback=g)
except BTLEException:
    print("Reconnecting..")
    band.connect(MAC)
    band.start_sensor_stream(heart_measure_callback=l, heart_raw_callback=b, accel_raw_callback=f, unknown_raw_callback=g)


band.disconnect()
