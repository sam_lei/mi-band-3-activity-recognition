___all__ = ['UUIDS']


class Immutable(type):

    def __call__(*args):
        raise Exception("You can't create instance of immutable object")

    def __setattr__(*args):
        raise Exception("You can't modify immutable object")


class UUIDS(object):

    __metaclass__ = Immutable

    BASE_A = "0000%s-0000-1000-8000-00805f9b34fb"
    BASE_B = "0000%s-0000-3512-2118-0009af100700"

    SERVICE_MIBAND1 = BASE_A % 'FEE0'
    SERVICE_MIBAND2 = BASE_A % 'FEE1'

    # GATT Services (specification)
    # https://www.bluetooth.com/specifications/gatt/services
    # Not working?
    GATT_ALERT = BASE_A % '1802'
    GATT_ALERT_NOTIFICATION = BASE_A % '1811'
    GATT_HEART_RATE = BASE_A % '180D'
    GATT_DEVICE_INFO = BASE_A % '180A'

    # Specific Xiaomi settings
    CHARACTERISTIC_NOTIFICATION = BASE_A % 'FF03'
    CHARACTERISTICS_CONTROL_POINT = BASE_A % 'FF05'
    SERVICE_LE_PARAMS = BASE_A % "FF09"
    CHARACTERISTIC_SENSOR_DATA = BASE_A % 'FF0E'

    SERVICE_ALERT = BASE_A % '2A06'
    SERVICE_CURRENT_TIME = BASE_A % '2A2B'
    SERVICE_HEART_RATE_MEASURE = BASE_A % '2A37'
    SERVICE_HEART_RATE_CONTROL = BASE_A % '2A39'
    SERVICE_AGE = BASE_A % '2A80'
    SERVICE_HEART_RATE_CONFIG = BASE_A % '2902'

    # https://github.com/Freeyourgadget/Gadgetbridge/blob/master/app/src/main/java/nodomain/freeyourgadget/gadgetbridge/devices/miband/MiBand2Service.java
    CHARACTERISTIC_ACCEL = BASE_B % '0001'
    CHARACTERISTIC_HZ = BASE_B % '0002'
    CHARACTERISTIC_CONFIGURATION = BASE_B % '0003'
    CHARACTERISTIC_BATTERY = BASE_B % '0006'
    CHARACTERISTIC_STEPS = BASE_B % '0007'
    CHARACTERISTIC_USER_SETTINGS = BASE_B % '0008'
    CHARACTERISTIC_AUTH = BASE_B % '0009'
    CHARACTERISTIC_DEVICEEVENT = BASE_B % '0010'

    CHARACTERISTIC_REVISION = 0x2a28
    CHARACTERISTIC_SERIAL = 0x2a25
    CHARACTERISTIC_HRDW_REVISION = 0x2a27

    NOTIFICATION_DESCRIPTOR = 0x2902


class NOTIFY(object):
    # NOTIFICATIONS: usually received on the UUID_CHARACTERISTIC_NOTIFICATION characteristic 

    NOTIFY_NORMAL = 0x0

    NOTIFY_FIRMWARE_UPDATE_FAILED = 0x1
    NOTIFY_FIRMWARE_UPDATE_SUCCESS = 0x2

    NOTIFY_CONN_PARAM_UPDATE_FAILED = 0x3
    NOTIFY_CONN_PARAM_UPDATE_SUCCESS = 0x4
    NOTIFY_AUTHENTICATION_SUCCESS = 0x5
    NOTIFY_AUTHENTICATION_FAILED = 0x6

    NOTIFY_FITNESS_GOAL_ACHIEVED = 0x7

    NOTIFY_SET_LATENCY_SUCCESS = 0x8

    NOTIFY_RESET_AUTHENTICATION_FAILED = 0x9
    NOTIFY_RESET_AUTHENTICATION_SUCCESS = 0xa

    NOTIFY_FW_CHECK_FAILED = 0xb
    NOTIFY_FW_CHECK_SUCCESS = 0xc

    NOTIFY_STATUS_MOTOR_NOTIFY = 0xd
    NOTIFY_STATUS_MOTOR_CALL = 0xe
    NOTIFY_STATUS_MOTOR_DISCONNECT = 0xf
    NOTIFY_STATUS_MOTOR_SMART_ALARM = 0x10
    NOTIFY_STATUS_MOTOR_ALARM = 0x11
    NOTIFY_STATUS_MOTOR_GOAL = 0x12
    NOTIFY_STATUS_MOTOR_AUTH = 0x13
    NOTIFY_STATUS_MOTOR_SHUTDOWN = 0x14
    NOTIFY_STATUS_MOTOR_AUTH_SUCCESS = 0x15
    NOTIFY_STATUS_MOTOR_TEST = 0x16

    # 0x18 is returned when we cancel data sync, perhaps is an ack for this message
    NOTIFY_UNKNOWN = -0x1
    NOTIFY_PAIR_CANCEL = 0xef
    NOTIFY_DEVICE_MALFUNCTION = 0xff


class MESSAGES(object):
    MSG_CONNECTED = 0x0
    MSG_DISCONNECTED = 0x1
    MSG_CONNECTION_FAILED = 0x2

    MSG_INITIALIZATION_FAILED = 0x3
    MSG_INITIALIZATION_SUCCESS = 0x4

    MSG_STEPS_CHANGED = 0x5
    MSG_DEVICE_STATUS_CHANGED = 0x6
    MSG_BATTERY_STATUS_CHANGED = 0x7


class COMMAND(object):
    #  COMMANDS: usually sent to UUID_CHARACTERISTIC_CONTROL_POINT characteristic 

    # Test HR
    COMMAND_SET_HR_SLEEP = 0x0
    COMMAND_SET_HR_CONTINUOUS = 0x1
    COMMAND_SET_HR_MANUAL = 0x2
    COMMAND_SET_REALTIME_STEPS_NOTIFICATION = 0x3
    COMMAND_SET_TIMER = 0x4
    COMMAND_SET_FITNESS_GOAL = 0x5
    
    COMMAND_FETCH_DATA = 0x6
    
    COMMAND_SEND_FIRMWARE_INFO = 0x7
    COMMAND_SEND_NOTIFICATION = 0x8

    # Unchecked
    COMMAND_FACTORY_RESET = 0x9
    
    COMMAND_CONFIRM_ACTIVITY_DATA_TRANSFER_COMPLETE = 0xa
    
    COMMAND_SYNC = 0xb
    
    COMMAND_REBOOT = 0xc
    
    COMMAND_SET_WEAR_LOCATION = 0xf
    
    COMMAND_STOP_SYNC_DATA = 0x11
    COMMAND_STOP_MOTOR_VIBRATE = 0x13

    COMMAND_SET_REALTIME_STEP = 0x10
    
    COMMAND_GET_SENSOR_DATA = 0x12


class AUTH_STATES(object):

    __metaclass__ = Immutable

    AUTH_OK = "Auth ok"
    AUTH_FAILED = "Auth failed"
    ENCRIPTION_KEY_FAILED = "Encryption key auth fail, sending new key"
    KEY_SENDING_FAILED = "Key sending failed"
    REQUEST_RN_ERROR = "Something went wrong when requesting the random number"


class ALERT_TYPES(object):

    __metaclass__ = Immutable

    NONE = b'\x00'
    MESSAGE = b'\x01'
    PHONE = b'\x02'


class QUEUE_TYPES(object):

    __metaclass__ = Immutable

    HEART = 'heart'
    RAW_HEART = 'raw_heart'
    ACCEL = 'accel'
    RAW_ACCEL = 'raw_accel'
    RAW_UNKNOWN = 'raw_unknown'
