# Mi Band 3 Activity Recognition

## Get raw IMU data
**Succeeded** in live streaming the data to PC over bluetooth but then Xiaomi changed their firmware und made hacking imppossible/very hard.
Project haltet. Code should weork for earlier Mi-Band firmware versions.


## Machine Learning on Time Series data for activity recognition
First visual validation showed good visibility of different sport exercises like push-ups, swimming, running, etc.

## Sources
* [Using Machine Learning for Real-time Activity Recognition
and Estimation of Energy Expenditure](http://alumni.media.mit.edu/~emunguia/pdf/PhDThesisMunguiaTapia08.pdf)
* [Xiaomi Mi Band data extraction, analytics and Google Fit sync tools](https://forum.xda-developers.com/general/accessories/xiaomi-mi-band-data-extraction-t3019156)
* [How I hacked my Xiaomi MiBand 2 fitness tracker — a step-by-step Linux guide](https://medium.com/machine-learning-world/how-i-hacked-xiaomi-miband-2-to-control-it-from-linux-a5bd2f36d3ad)
